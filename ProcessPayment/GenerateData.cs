﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ProcessPayment
{
    public class GenerateData
    {
        private static readonly IList<string> data = new List<string>();
        private static string filePath;

        public static void RunGen(int numOfRecs, string path)
        {
            filePath = path;
            for (int ct = 0; ct < numOfRecs; ct++)
                GenData();
            WriteToFile();
        }

        private static void GenData()
        {
            Random random = new Random();
            double value = random.NextDouble(50, 100);
            string items = Math.Round(value, 2).ToString() + ",";
            for (int ct = 0; ct < 10; ct++)
            {
                items += Math.Round(random.NextDouble(1, 15), 2).ToString();
                if (ct < 9)
                    items += ",";
            }
            data.Add(items);
        }

        private static void WriteToFile()
        {
            File.WriteAllLines(filePath, data.ToArray());
        }
    }
}
