﻿namespace ProcessPayment
{
    public class ProcessPayments
    {
        private string dataPath;

        public ProcessPayments(string DataPath)
        {
            dataPath = DataPath;
        }

        public void StartProcess()
        {
            string line;
            double[] payments;

            StreamReader file = new StreamReader(dataPath);
            while ((line = file.ReadLine()) != null)
            {
                payments = getData(line);
                procData(payments);
            }
            file.Close();
        }

        private double[] getData(string s)
        {
            string[] numbers = s.Split(',');
            double[] payments = new double[numbers.Length];
            for (int i = 0; i < payments.Length; i++)
            {
                payments[i] = Convert.ToDouble(numbers[i]);
            }
            return payments;
        }

        private void procData(double[] payments)
        {
            string result;
            double balance = payments[0];
            double totalPayments = 0;

            for (int x = 1; x < payments.Length; x++)
            {
                totalPayments += payments[x];
                if (totalPayments >= balance)
                    break;
            }
            if (totalPayments < balance)
                result = $"Balance due ${Math.Round(balance - totalPayments, 2)}";
            else if (totalPayments > balance)
                result = $"Change due ${Math.Round(totalPayments - balance, 2)}";
            else
                result = "Balance paid in full";
            outPutResults(result);
        }

        private void outPutResults(string result)
        {
            Console.WriteLine(result);
        }
    }
}
