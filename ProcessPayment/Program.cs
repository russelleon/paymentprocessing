﻿namespace ProcessPayment
{
    class Program
    {
        private static readonly string filePath = Environment.CurrentDirectory + "\\PaymentData.txt";
        private static readonly int numOfRecs = 20;

        static void Main(string[] args)
        {
            GenerateData.RunGen(numOfRecs, filePath);
            ProcessPayments procPay = new ProcessPayments(filePath);
            procPay.StartProcess();
            Console.ReadKey();
        }
    }
}
